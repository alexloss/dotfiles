#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

[[ -f ~/.welcome_screen ]] && . ~/.welcome_screen
[[ -f ~/.colors ]] && . ~/.colors
[[ -f ~/.config/aliases ]] && . ~/.config/aliases


PS1=\
'${RESET}[${MAGENTA}\u${RESET}@${YELLOW}\h${RESET}]${RED}$(_disp_exit_code)${GRAY}$(_disp_jobs)\n'\
'\[${BLUE}\]\w\[${GREEN}\]$(__fastgit_ps1)\[${RESET}\] \$ '
# Note: \[ ... \] is required to tell the terminal the escaped sequence is 0 width

alias ls='lsd'
alias ll='ls -lAv'   # show long listing of all except ".."
alias l='ls -lAv'   # show long listing but no hidden dotfiles except "."
alias la='ls -A'

[[ "$(whoami)" = "root" ]] && return

[[ -z "$FUNCNEST" ]] && export FUNCNEST=100          # limits recursive functions, see 'man bash'

## Use the up and down arrow keys for finding a command in history
## (you can write some initial letters of the command first).
bind '"\e[A":history-search-backward'
bind '"\e[B":history-search-forward'

################################################################################
## Some generally useful functions.

# number of running background jobs
alias rjobs="jobs -rp  |  wc -l"
# number of stopped background jobs
alias sjobs="jobs -sp  |  wc -l"

_disp_jobs () {
	# Helper function for PS1
	[ $(rjobs) -gt 0 ] && printf " [running $(rjobs)]"
	[ $(sjobs) -gt 0 ] && printf " [stopped $(sjobs)]"
}

_disp_exit_code() {
	# Helper function for PS1
	_ret="$?"
	[ $_ret -ne 0 ] && printf " [exit code $_ret]"

}

__fastgit_ps1 () {
    # Faster than git-branch 
    local headfile head branch
    local dir=$(pwd)

    while [ "$dir" != "/" ]; do
        if [ -e "$dir/.git/HEAD" ]; then
            headfile="$dir/.git/HEAD"
            break
        fi
	dir="$(dirname $dir)"
    done

    if [ -e "$headfile" ]; then
        read -r head < "$headfile" || return
        case "$head" in
		ref:*) branch="$(echo $head | sed 's/ref: refs\/heads\///')" ;;
            "") return 0 ;; # No branches?
            *) branch="${head:0:7}" ;;  #Detached head
        esac
    else
	return 0
    fi
    printf " (%s)" "$branch"
}

_CheckInternetConnection() {
    # curl --silent --connect-timeout 8 https://8.8.8.8 >/dev/null
    eos-connection-checker
    local result=$?
    test $result -eq 0 || echo "No internet connection!" >&2
    return $result
}
