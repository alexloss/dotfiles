export PATH="$PATH:/home/alex/.local/bin"

export BROWSER="firefox"
export TERMINAL="alacritty"

alias vim="nvim"

